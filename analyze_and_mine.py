import google_trends
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import *
from sklearn.metrics import *
from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
from sklearn.decomposition import PCA
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import *
class_labels = ['Explainers', 'Politics & Policy', 'World', 'Culture', 'Science & Health',
                    'Identities', 'Energy & Environment', 'The Big Idea', 'Technology',
                    'Business & Finance', 'Video']


def heuristic_baseline_classifier(x_all, y_all):
    print("Running our baseline heuristic classifier which predicts most frequent label regardless of the input...")
    Xtr, Xte, Ytr, Yte = train_test_split(x_all, y_all, test_size=0.3, random_state=4)
    label_dict = {}
    for label in Ytr:
        if label in label_dict:
            label_dict[label] += 1
        else:
            label_dict[label] = 1

    max_key = max(label_dict, key=(lambda key: label_dict[key]))
    pred = np.full((Xte.shape[0],), max_key)
    print("Accuracy of our Heuristic dumb classifier: " + str(np.mean(pred == Yte)) + "\n")


def compare_classifiers(x_all, y_all_reduced):
    global class_labels
    print("Comparing different classifiers for generating metric...")
    Xtr, Xte, Ytr, Yte = train_test_split(x_all, y_all_reduced, test_size=0.3, random_state=4)

    print("Running One vs Rest classifier...")
    clf = OneVsRestClassifier(LinearSVC())
    clf.fit(Xtr, Ytr)
    print("Accuracy of OVR Classifier: " + str(clf.score(Xte, Yte)) + "\n")

    print("Running All vs All classifier...")
    clf = OneVsOneClassifier(LinearSVC())
    clf.fit(Xtr, Ytr)
    print("Accuracy of AVA Classifier: " + str(clf.score(Xte, Yte)) + "\n")

    print("Running Naive Bayes (Multinomial) classifier...")
    clf = MultinomialNB()
    clf.fit(Xtr, Ytr)
    print("Accuracy of Naive Bayes Classifier: " + str(clf.score(Xte, Yte)) + "\n")

    print("Running Naive Bayes (Bernoulli) classifier...")
    clf = BernoulliNB()
    clf.fit(Xtr, Ytr)
    print("Accuracy of Naive Bayes Classifier: " + str(clf.score(Xte, Yte)) + "\n")


def transform_tfidf(data_frame):
    tvec = TfidfVectorizer(stop_words='english')
    body = data_frame["body"]
    category = data_frame["category"]
    x_all = tvec.fit_transform(body).toarray()
    y_all = np.array(category)
    return x_all, y_all


def prediction(x_all, y_all):
    global class_labels
    # class_labels = ['Explainers', 'Politics & Policy', 'World', 'Culture', 'Science & Health',
    #                 'Identities', 'Energy & Environment', 'The Big Idea', 'Technology',
    #                 'Business & Finance', 'Video']
    f1, precision, recall = (np.zeros(shape=(11,), dtype=float) for i in range(3))
    accuracy = []
    print("Using Naive Bayes as the classifier for evaluating our metric...")
    print("Running 5-Fold Validation...")
    fold_no = 1
    # kf = KFold(n_splits=5, shuffle=True)
    # for train, test in kf.split(x_all):
    skf = StratifiedKFold(n_splits=5)
    for train, test in skf.split(x_all, y_all):
        print("\tRunning for fold - " + str(fold_no))
        m = BernoulliNB()
        m.fit(x_all[train], y_all[train])
        y_true = y_all[test]
        y_pred = m.predict(x_all[test])
        # target_names = ['1','0', '-1']
        # cr = classification_report(sentiment.Y[test], m.predict(sentiment.X[test]), target_names=target_names)
        accuracy.append(m.score(x_all[test], y_all[test]))
        precision += precision_score(y_true, y_pred, average=None, labels=class_labels)
        recall += recall_score(y_true, y_pred, average=None, labels=class_labels)
        f1 += f1_score(y_true, y_pred, average=None, labels=class_labels)
        fold_no += 1

    print("\t All Folds Completed!")
    print("Computing Average of all 5 folds...")
    precision /= 5
    recall /= 5
    f1 /= 5
    print("Printing...")

    from astropy.table import Table, Column
    t = Table(names=(' ',  'Explainers', 'Politics & Policy', 'World', 'Culture', 'Science & Health',
                     'Identities', 'Energy & Environment', 'The Big Idea', 'Technology',
                     'Business & Finance', 'Video'),
              dtype=('S10', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4'))
    t.add_row(('Precision', precision[0], precision[1], precision[2], precision[3], precision[4], precision[5]
               , precision[6], precision[7], precision[8], precision[9], precision[10]))
    t.add_row(('Recall', recall[0], recall[1], recall[2], recall[3], recall[4], recall[5], recall[6]
               , recall[7], recall[8], recall[9], recall[10]))
    t.add_row(('F1-Score', f1[0], f1[1], f1[2], f1[3], f1[4], f1[5], f1[6], f1[7], f1[8], f1[9], f1[10]))
    print("\n")
    print(t)
    print("\n\n")
    print("OVERALL ACCURACY:   " + str(sum(accuracy)/5))
    print("\n")


def train_vectorizer(data_frame):
    print("Training TF-IDF for all months from 2014-03  to  2017-03")
    print("Estimating feature names by weights...")
    dict_vectorizer = {}
    dates_list = google_trends.dates_list
    for key in dates_list:
        # data_list = data_frame.loc[data_frame["published_date"] == key, "body",].tolist()
        # descending_weight_list = []
        tvec = TfidfVectorizer(stop_words='english', ngram_range=(1, 3))
        # tvec = TfidfVectorizer(stop_words='english', ngram_range=(1, 3))
        filtered_body = data_frame.loc[data_frame["published_date"] == key, "body",].tolist()
        tvec_weights = tvec.fit_transform(filtered_body)
        weights = np.asarray(tvec_weights.mean(axis=0)).ravel().tolist()
        weights_df = pd.DataFrame({'keyword': tvec.get_feature_names(), 'weight': weights})
        # descending_weight_list.append(weights_df.sort_values(by='weight', ascending=False).head(10)['keyword'].tolist())
        dict_vectorizer[key] = weights_df.sort_values(by='weight', ascending=False).head(100)['keyword'].tolist()
    print("Estimation complete. Printing top 10 features having most weights for 1st 5 months")
    for index in range(5):
        print(dates_list[index] + " - " + str(dict_vectorizer[dates_list[index]][:5]))
    print("\n")
    return dict_vectorizer


def hit_or_miss(dict_trends, dict_vectorizer):
    print("Comparing Vectorized data with trending data...")
    print("Calculating \"Hit or Miss\" for each month.")
    vector = np.zeros(len(dict_trends),)
    index = 0
    for key in dict_trends.keys():
        trends = dict_trends[key]
        weight_vector = dict_vectorizer[key]

        # for item in weight_vector:
        #     if item in trends:
        #         vector[index] = 1
        #         break
        for item in weight_vector:
            if any(item in s.split() for s in trends):
                vector[index] = 1
                break
        index += 1
    print("Resulting Vector: " + str(vector))
    print("Resulting Mean / Hit Ratio : " + str(np.mean(vector)*100))
    print("\n")
    return vector


