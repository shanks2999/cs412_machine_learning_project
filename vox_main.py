import data_preprocessing
import analyze_and_mine
import google_trends
import datetime
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

dates_list_static = ['201403', '201404', '201405', '201406', '201407', '201408', '201409',
                   '201410', '201411', '201412', '201501', '201502', '201503', '201504',
                   '201505', '201506', '201507', '201508', '201509', '201510', '201511',
                   '201512', '201601', '201602', '201603', '201604', '201605', '201606',
                   '201607', '201608', '201609', '201610', '201611', '201612', '201701',
                   '201702', '201703']


def main():
    start_time = datetime.datetime.now()
    """     Pre-processing steps for getting google trends data     """
    # dict_trends = google_trends.trends_by_date()
    # google_trends.load_trends_to_csv(dict_trends)

    """     Pre-processing steps for cleaning, reducing features, and organizing Vox data     """
    # data_frame = data_preprocessing.load_from_original_csv('dsjVoxArticles.tsv')
    # data_frame = data_preprocessing.data_cleansing(data_frame)
    # data_frame = data_preprocessing.prepare_for_tfidf(data_frame)
    # data_preprocessing.load_to_reduced_csv(data_frame)

    """     Getting reduced dataset    """
    dict_trends = google_trends.load_trends_from_csv()
    data_frame = data_preprocessing.load_from_reduced_csv('VoxArticlesReduced.csv')

    # """     Now Transforming and predicting Hit and Miss for each month    """
    dict_vectorizer = analyze_and_mine.train_vectorizer(data_frame)
    hit_missvector = analyze_and_mine.hit_or_miss(dict_trends, dict_vectorizer)



    """     Transform the data  """
    x_all, y_all = analyze_and_mine.transform_tfidf(data_frame)
    analyze_and_mine.heuristic_baseline_classifier(x_all, y_all)

    y_all_reduced = data_preprocessing.feature_reduction(y_all)

    """     Comparing classifiers to get different accuracies  """
    # analyze_and_mine.compare_classifiers(x_all, y_all_reduced)

    """     Now Learning and Classifying data with categories    """
    analyze_and_mine.prediction(x_all, y_all_reduced)

    end_time = datetime.datetime.now()
    print("DONE!")
    print("Total time taken: " + str(end_time - start_time))


if __name__ == "__main__":
    main()
