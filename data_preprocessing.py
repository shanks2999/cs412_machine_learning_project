import csv
import pandas as pd
import numpy as np
from textblob import TextBlob
import nltk
from multiprocessing import Pool


def load_from_original_csv(file_path):
    print("Loading data from Original VOX CSV...")
    data_frame = pd.read_table(
        file_path, header=0,
        names=['title', 'author', 'category', 'published_date',
               'updated_on', 'slug', 'blurb', 'body'],
        skip_blank_lines=True,
        parse_dates=[3,4],
        encoding='utf-8'
        )
    print("VOX data loaded!\n")
    return data_frame


def data_cleansing(data_frame):
    print("Cleaning original data...")
    # data_frame['published_date'] = data_frame['published_date'].dt.date
    # data_frame['updated_on'] = data_frame['updated_on'].dt.date
    print("Converting date to month...")
    data_frame['published_date'] = data_frame['published_date'].dt.strftime('%Y%m')
    # data_frame['updated_on'] = data_frame['updated_on'].dt.strftime('%Y-%m')
    print("Stripping HTML from body...")
    data_frame['body'].replace(to_replace='<[^<]+?>|[^\x00-\x7F]+|[\"|\/]+|\\\\n', value='', regex=True, inplace=True)
    # data_frame["body"] = [s.encode('ascii', 'ignore').strip() for s in pd.Series.str]
    data_frame['slug'].replace(to_replace='.*\/|\-', value=' ', regex=True, inplace=True)

    print("Sorting data by month...")
    data_frame.sort_values(by='published_date', inplace=True)
    data_frame = data_frame.dropna(axis=0, how='any')
    return data_frame

def feature_reduction(Y):
    print("Reducing the large number of categories i.e. 187")
    dict_features = load_feature_dictionary()
    category_list = Y[:].tolist()
    for index in range(len(category_list)):
        for key in dict_features.keys():
            if key in category_list[index]:
                category_list[index] = dict_features[key]
                break
    import warnings
    warnings.filterwarnings("ignore")
    print("Reduction complete to ~11...\n")
    return np.array(category_list[:])

def compute_nltk_Corpus(sentence_list, core_no):
    nltk_tag = ['NN', 'NNS', 'NNP', 'NNPS', 'PRP']
    print("Removing POS TAGS from body using Core - " + str(core_no))

    # for index in range(len(body_list)):
    #     tagged_sentence = nltk.tag.pos_tag(body_list[index].split())
    #     edited_sentence = [word for word, tag in tagged_sentence if tag in nltk_tag]
    #     body_list[index] = ' '.join(edited_sentence)
    # data_frame["body"] = pd.Series(body_list[:])

    for index in range(len(sentence_list)):
        blob = TextBlob(sentence_list[index])
        sentence_list[index] = ' '.join([n for n,t in blob.tags if t == 'NN' and len(n) > 2])

    # for index in range(len(sentence_list)):
    #     blob = TextBlob(sentence_list[index])
    #     sentence_list[index] = ' '.join(blob.noun_phrases)
    print("Process Completed for Core - " + str(core_no))
    return sentence_list[:]


def prepare_for_tfidf(data_frame):
    print("Pre-Processing data for TF-IDF...")
    body_list = data_frame['body'].tolist()
    div = int(len(body_list)/2)
    # a = datetime.datetime.now()
    pool = Pool(processes=2)
    print("Initiate Multi-processing using 2 Cores...\nThis will take few minutes...")
    result1 = pool.apply_async(compute_nltk_Corpus, args=(body_list[0:div], 1,))
    result2 = pool.apply_async(compute_nltk_Corpus, args=(body_list[div:], 2,))
    r1 = result1.get()
    r2 = result2.get()
    # b = datetime.datetime.now()
    # print(b - a)
    body_list_new = r1[:]
    body_list_new.extend(r2[:])
    data_frame["body"] = pd.Series(body_list_new[:])
    print("Data pre-processing is complete!\n")
    return data_frame



def load_feature_dictionary():
    print("\tLoading feature dictionary...")
    dict_features = {}
    with open('VoxCategory.csv', 'r', newline='', encoding='utf-8') as file:
        reader = csv.reader(file)
        header = next(reader)
        for row in reader:
            for index in range(len(header)):
                if row[index] != '':
                    dict_features[row[index]] = header[index]
        print("\tDone!")
        return dict_features


def load_to_reduced_csv(data_frame):
    print("Loading all pre-processed data to CSV...")
    category = data_frame["category"].tolist()
    published_date = data_frame["published_date"].tolist()
    title = data_frame["title"].tolist()
    slug = data_frame["slug"].tolist()
    blurb = data_frame["blurb"].tolist()
    body = data_frame["body"].tolist()
    with open('VoxArticlesReduced.csv', 'w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerow(['category', 'published_date', 'title', 'slug', 'blurb', 'body'])
        for index in range(len(category)):
            writer.writerow([category[index], published_date[index], title[index],
                             slug[index], blurb[index], body[index]])
        print("All data loaded to CSV named - \"VoxArticlesReduced.csv\"\n")


def load_from_reduced_csv(file_path):
    print("Loading all pre-processed data from CSV named - \"VoxArticlesReduced.csv\"...")
    data_frame = pd.read_csv(
        'VoxArticlesReduced.csv',
        header=0,
        names=['category', 'published_date', 'title', 'slug', 'blurb', 'body'],
        dtype={'category': str, 'published_date': str,'title': str, 'slug': str, 'blurb': str, 'body': str},
        encoding='utf-8')
    data_frame = data_frame.dropna(axis=0, how='any')
    print("Loading complete!\n")
    return data_frame