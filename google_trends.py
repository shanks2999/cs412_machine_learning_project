from pytrends.request import TrendReq
import pandas as pd
import numpy as np
import csv

google_trends = ['actors', 'animals', 'athletes', 'authors', 'baseball_players', 'baseball_teams',
                 'basketball_players', 'basketball_teams', 'books', 'cities', 'dog_breeds', 'financial_companies',
                 'foods', 'games', 'people', 'politicians', 'reality_shows', 'retail_companies', 'scientists',
                 'soccer_players', 'soccer_teams', 'songs', 'sports_cars', 'sports_teams', 'teen_pop_artists',
                 'tv_shows']

dates_list = ['201403', '201404', '201405', '201406', '201407', '201408', '201409',
              '201410', '201411', '201412', '201501', '201502', '201503', '201504',
              '201505', '201506', '201507', '201508', '201509', '201510', '201511',
              '201512', '201601', '201602', '201603', '201604', '201605', '201606',
              '201607', '201608', '201609', '201610', '201611', '201612', '201701',
              '201702', '201703']


def trends_by_date():
    global google_trends, dates_list
    print("Getting trends month by month...")
    pytrend = TrendReq()
    dict_trends = {}

    for yyyymm in dates_list:
        trending = []
        for trend in google_trends:
            try:
                top_trends = pytrend.top_charts(cid=trend, date=int(yyyymm))
                trending.extend(top_trends['title'].tolist()[:10])
            except:
                pass
        dict_trends[yyyymm] = list(set(trending[:]))
    print("All trends fetched!\n")
    return dict_trends


def load_trends_to_csv(dict_trends):
    global google_trends, dates_list
    print("Loading fetched trends to CSV...")
    with open('GoogleTrends.csv', 'w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerow(['yyyymm', 'trending'])
        for key in dict_trends.keys():
            list = dict_trends[key]
            for value in list:
                writer.writerow([key, value])
    print("All trends loaded to CSV \"GoogleTrends.csv\"!\n")


def load_trends_from_csv():
    global google_trends, dates_list
    print("Loading previously written trends from CSV - \"GoogleTrends.csv\"")
    dict_trends = {}
    with open('GoogleTrends.csv', 'r', newline='', encoding='utf-8') as file:
        reader = csv.reader(file)
        header = next(reader)
        for row in reader:
            if row[0] in dict_trends:
                dict_trends[row[0]].append(row[1])
            else:
                dict_trends[row[0]] = [row[1]]
        print("All trends are now loaded !\n")
        return dict_trends
