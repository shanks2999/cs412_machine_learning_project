**Sentiment Analysis on Vox News Corpus**

Link to dataset: https://data.world/elenadata/vox-articles

This is just the basic ReadMe. 

To get more detailed info on what the project is actually about, I highly encourage you to check the Python Notebook "Vox_Classification_Trends.ipynb". 

It shows the actual logic and the output to better understand the approach behind the implementation.


Basic Overview:

	> Used pytrends to get google trending data by month.
	> Did preprocessing on vox dataset to make it more meaningful to the classifier.
	> Computed Feature weights from Vox dataset and compare with the Google Trending data to analyze the content covered by the Vox News.
	> Used TF IDF vectorizer of python to convert data into "Bag of Words" model.
	> Train different classifiers on dataset to predict the category of data using a heuristic classifier.
	> Reduced the categories using categories from the vox wesite: https://www.vox.com
	> Trained various classifiers and picked which worked optimal compared to out baseline heuristic classifier.
	> Evauated our classifier using 5-Fold cross validation(Computing Precision, Recall and F-Score of all categories)


Cheers,

Shashank
